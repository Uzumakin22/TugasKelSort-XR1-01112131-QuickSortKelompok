/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quicksortkelompok;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class QuickSortKelompok {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        // TODO code application logic here
        
        tampilkanJudul();
        
        Scanner scan = new Scanner(System.in);
        
        // Input jumlah Data
        System.out.print("Masukan Jumlah Data Nilai Rapot : ");
        int jlh_data = scan.nextInt();
        
        // Input nilai tiap data
        int[] data = new int[jlh_data];
        
        // Array untuk nilai tiap Data  
        System.out.println();
        for(int x = 0; x < jlh_data; x++)
        {
            System.out.print("Masukan Nilai Mata Pelajaran ke "+(x+1)+" : ");
            data[x] = scan.nextInt();
        }
        System.out.print("\nData Rapot Sebelum di sorting : "); 
        for(int x = 0; x < jlh_data; x++)
        {
            System.out.print(data[x]+" ");
        }    
        quickSort(data, 0, data.length - 1);
        
        System.out.print("\n\nHasil Pengurutan Nilai : ");
        tampilkanLarik(data, data.length);
        
    }
    
    public static void quickSort(int[] data, int p, int r) 
    {
        int q = partisi(data, p, r);
        
        if (p < r) 
        {
            quickSort(data, p, q);
            quickSort(data, q+1, r);
        }
        
    }
    
    public static int partisi(int[] data, int p, int r) 
    {
        int x, i, j, tmp;
        
        x = data[p];
        i = p;
        j = r;
        
        while (true) 
        {
            while (data[j] > x)
                j--;
                
            while (data[i] < x)
                i++;

            if (i < j) 
            {
                // Tukarkan data
                tmp = data[i];
                data[i] = data[j];
                data[j] = tmp;
            }            
            else
                return j;
        }
        
    }
    
    public static void tampilkanLarik(int[] data, int n) 
    {
        for (int i = 0; i < n; i++)
        {
            System.out.printf("%d ", data[i]);
        }            
    }

    private static void tampilkanJudul() 
    {
        System.out.println("Mengurutkan Nilai Rapot Dari Yang Terkecil");
        System.out.println("------------------------------------------\n");
        System.out.println("Nama Kelompok 1 : ");
        System.out.println("Achmad Syeh Hasimy Md/01");
        System.out.println("Bima Eka Putra Sakti/11");
        System.out.println("Harry Merlien Fadhlurrohman/21");
        System.out.println("Nabilla Maysha Pranasty/31\n");
        
    }
    
}
   
 
    
    
